function drawbodies(handle,bodies,destination,jointSize, limbsThickness)
    % drawBodies - Draw bodies on depth image
    % Input Parameters: 
    % 1) handle: image axes handle
    % 2) bodies: bodies structure returned by getBodies method
    % 3) destination: destination image (depth or color)
    % 4) jointsSize: joints' size (circle raddii)
    % 5) bonesThickness: Bones' Thickness
    % Output: none
    % See bodyTrackingDemo.m
    numBodies = size(bodies,2);
    bodyColors = ['r','b','g','y','m','c','r','b','g','y','m','c','r','b','g','y','m','c'];
    
    % Draw the limbs
    bodyLimbs = [27 4; 4 3; 3 2; 2 1; 4 5; 4 12; 5 6; 12 13; ...
                  6 7; 13 14; 7 8; 14 15; 8 11; 15 18; 8 9; ...
                  15 16; 9 10; 16 17; 1 19; 1 23; 19 20; ...
                  23 24; 20 21; 24 25; 21 22; 25 26];
    
    % Draw each body
    for i=1:numBodies                
        if strcmp(destination,'depth')
            % Get the joints in depth image space
            pos2D = bodies(i).Position2d_depth;
        elseif strcmp(destination,'color')
            pos2D = bodies(i).Position2d_rgb;
        elseif strcmp(destination,'lateral')
            pos3D = bodies(i).Position3d;
            pos2D = pos3D(2:3,:);
            pos2D(2,:) = -pos2D(2,:);
        end
        
        % Select the color with the Id
        body_id = bodies(i).Id;
        
        % Draw the joints
%                 hold(handle,"on")
%                 scatter(handle,pos2D(1,:),pos2D(2,:),jointsSize*10,...
%                     'MarkerEdgeColor',this.bodyColors(body_id),...
%                     'MarkerFaceColor','w',...
%                     'LineWidth',1.5);
%                 hold(handle,"off")
        if ~isempty(body_id)
         % Draw the limbs
         for j=1:length(bodyLimbs)
             joint1 = bodyLimbs(j, 1);
             joint2 = bodyLimbs(j, 2);
             xs = [pos2D(1,joint1) pos2D(1,joint2)];
             ys = [pos2D(2,joint1) pos2D(2,joint2)];
             line(xs, ys, 'Color', bodyColors(body_id), ...
                 'LineWidth', limbsThickness, 'Parent', handle);
         end
        end
    end
end