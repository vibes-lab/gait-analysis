% % Data Acquisition Live
% clear all; close all; clc;
% 
% devices = daq.getDevices % Do this once to check the DAQ system.

% MAKE SURE YOU ARE COLLECTING AND SAVING DATA FROM ALL SENSORS.
% REMEMBER TO CHANGE THE FILENAME FOR EVERY TRIAL BEFORE RUNNING THE CODE.
% MAKE SURE YOU ARE SAVING DATA FROM ALL SENSORS.

%% Before Run Check-List

trial_prompt = "Trial(press ENTER to keep the previous value): ";
trial_temp = input(trial_prompt);

while trial_temp ~= isempty(trial_temp)
    trial = ['Trial_', num2str(trial_temp), '_'];
    break;
end

name_prompt = "Name ('Rafa', 'Will', 'Santi', or someone else): ";
name_temp = upper(input(name_prompt, 's'));

while name_temp ~= isempty(name_temp)
    if  name_temp == 'R'
        name = 'Rafael_';
    elseif name_temp == 'W'
        name = 'Will_';
    elseif name_temp == 'S'
        name = 'Santi_';
    else
        name = [lower(name_temp), '_'];
    end
    break;
end

speed_prompt = "Speed ('fast' or 'regular'): ";
speed_temp = upper(input(speed_prompt, 's'));

while speed_temp ~= isempty(speed_temp)
    if speed_temp == 'F'
        speed = 'Fast_Gait_';
    elseif speed_temp == 'R'
        speed = 'Regular_Gait_';
    else
        speed = [lower(speed_temp), '_Gait_'];
    end
    break;
end

weight_prompt = "Weight ('extra' or 'body'): ";
weight_temp = upper(input(weight_prompt, 's'));

while weight_temp ~= isempty(weight_temp)
    if weight_temp == 'E'
        weight = 'Extra_Weight';
    elseif weight_temp == 'B'
        weight = 'Body_Weight';
    else
        weight = [lower(weight_temp), '_Weight'];
    end
    break;
end

clear('*_prompt', '*_temp');

save_file = [trial, name, speed, weight];

%% Acquiring Data

s = daq.createSession('ni');  %Creates Session

% Adding analog channels

% cDAQ 1
sensor_1 = addAnalogInputChannel(s, '1EE3DBFMod1', 0, 'IEPE');  
sensor_3 = addAnalogInputChannel(s, '1EE3DBFMod1', 1, 'IEPE');
sensor_5 = addAnalogInputChannel(s, '1EE3DBFMod1', 2, 'IEPE');
% sensor_7 = addAnalogInputChannel(s, '1EE3DBFMod1', 3, 'IEPE');
% 
% % cDAQ 2
% sensor_2 = addAnalogInputChannel(s, '1EEE4C4Mod1', 0, 'IEPE');  
% sensor_4 = addAnalogInputChannel(s, '1EEE4C4Mod1', 1, 'IEPE');
% sensor_6 = addAnalogInputChannel(s, '1EEE4C4Mod1', 2, 'IEPE');

% Samping rate in samples/second
s.Rate = 16384; 

% Running time in seconds.
s.DurationInSeconds = 15; 

disp('Starting Acquisition...');
lh = addlistener(s,'DataAvailable', @(src,event) plot(event.TimeStamps, event.Data)); % Shows data live
s.NotifyWhenDataAvailableExceeds = 2048; % Change here the live feed

[data, time]= s.startForeground();

delete(lh)

%% Extracting Data

% cDAQ 1
sensor_1 = data(:,1);
sensor_3 = data(:,2);
sensor_5 = data(:,3);
% sensor_7 = data(:,4);
% 
% % cDAQ 2
% sensor_2 = data(:,5);
% sensor_4 = data(:,6);
% sensor_6 = data(:,7);

%% Plotting Data

% cDAQ 1
figure(1); hold on;
plot(time,sensor_1)
% legend("Sensor 1")

% figure(3)
% plot(time,sensor_2)
% legend("Sensor 2")

% figure(4)
plot(time,sensor_3)
% legend("Sensor 3")

% figure(5)
% plot(time,sensor_4)
% legend("Sensor 4")

% cDAQ 2
% figure(5);  % hold on;
plot(time,sensor_5)
% legend("Sensor 5")

% figure(6)
% plot(time,sensor_6)
% legend("Sensor 6")

% figure(7)
% plot(time,sensor_7)
% legend("Sensor 7")

% figure(8)
% plot(time,daq2_ch3)
% % legend("Sensor 8")

legend("Sensor 1", "Sensor 3",  "Sensor 5")

%% Timestamps

num_sec = 4;

sec_1_temp = "Section 1 time: ";
sec_2_temp = "Section 2 time: ";
sec_3_temp = "Section 3 time: ";
sec_4_temp = "Section 4 time: ";

sec_temp = [sec_1_temp, sec_2_temp, sec_3_temp, sec_4_temp];

section_times = zeros(num_sec, 1);

for i = 1:num_sec
    section_times(i) = input(sec_temp(i));
    if section_times(i) == isempty(section_times(i))
        section_times(i) = 0;
    end
end

clear('*_temp');

%% Saving Data

save(save_file,'sensor_1','sensor_2','sensor_3','sensor_4','sensor_5','sensor_6','sensor_7','time', 'section_times')

close all;

