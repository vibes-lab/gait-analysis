# In Situ Gait Analysis

The in situ gait analysis system is designed to collect patient gait data passively, that is, without requiring any actions for the patient other than normal walking. It is also designed to be inexpensive and deployable in any open walking area, like a long hallway. It consists of two sets of sensors: 1) an Azure Kinect Depth Camera and 2) a set of seven accelerometers. The depth camera serves to collect whole body motion, while the accelerometers accurately measure footstep timing and can also roughly estimate the walker's position and speed.

Below you will find detailed instructions on how to deploy the system and operate the main GUI.

## Deployment



## Operation

## Troubleshooting
