function [cleanData, tRange] = cleanKinzData(rawData)

%% Find number of single dropouts
% a single dropout is [1,0,1] pattern in numbodies
nFrames = length(rawData); %number of Kinect frames
singleDropout = false(1,nFrames); %vector denoting single frame dropouts
%(to be interpolated)
numbodies = [rawData(:).numbodies]; %numbodies array

%search for pattern
for k = 2:(nFrames-1)
    if numbodies(k) == 0 && numbodies(k-1) == 1 && numbodies(k+1) == 1
        singleDropout(k) = true;
    end
end

%% Interpolate to replace dropout points
dropoutI = find(singleDropout);
rawDataInt = rawData;
for i = 1:length(dropoutI)
    rawDataInt = kinzInterp(rawData,dropoutI(i));
end

%% Find continuous body periods
numbodies = [rawDataInt(:).numbodies]; %numbodies array (for interpolated data)
change = diff([0 numbodies]); %change in number of bodies found
bodyPeriod = zeros(nFrames,2); %stores indices of continuous periods where 
% a body was found
count = 0; %period counter
for k = 1:nFrames
    if change(k) > 0
        %if there is a body found
        count = count + 1; %increment the count
        bodyPeriod(count,1) = k; %mark as start of body period
    elseif change(k) < 0
        %if there is a body lost
        bodyPeriod(count,2) = k-1; %mark as end of body period
    elseif numbodies(k) == 1 && k == nFrames
        %if there is still a body in the last frame
        bodyPeriod(count,2) = k; %mark as end of body period
    end
end
bodyPeriod = bodyPeriod(1:count,:);

%% Find the longest period in the data 
if ~isempty(bodyPeriod)
    periodDur = diff(bodyPeriod,1,2)+1; %period durations
    [~,maxI] = max(periodDur);
    
    [time_sec, ~] = kinz_time(rawDataInt);
    periodRange = bodyPeriod(maxI,1):bodyPeriod(maxI,2);
    tRange = time_sec(periodRange);
    maxDur = tRange(end)-tRange(1);
else
    maxDur = 0;
end

if maxDur > 1 %if duration is more than 1 seconds
    %output the data    
    cleanData = rawDataInt(periodRange);    
else
    %output empty data
    cleanData = [];
    tRange = [];
    disp('Kinz Clean: bodies duration too short for analysis.')
end

