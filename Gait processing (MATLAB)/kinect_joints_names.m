function [joint_names, joint_ids] = kinect_joints_names(data)
% Extract the names of all tracked kinect joints during the trial.

joints_num = length(data.joint_names);  % Num of joints

joint_names = "";

for i = 1:joints_num
    joint_names(i) = string(data.joint_names(i));
end

joint_names = joint_names';
joint_ids = 1:length(joint_names);
end