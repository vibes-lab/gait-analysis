function [joint_positions] = kinect_joints_positions(data)
% Extract the position of every tracked joing and save them in a matrix.

% Get available joints
joints = kinect_joints_names(data);

frames_num = length(data.frames); % Total number of frames 
joints_num = length(joints); % Total number of joints

joint_positions = zeros(frames_num, 3, joints_num); % 3 columns because 
% joint positions are given in xyz.

for i = 1:frames_num
    for j = 1:joints_num
        dat = data.frames(i);
        if isempty(dat.bodies)
            joint_positions(i, :, j) = 0;
        else
            joint_positions(i,:,j) = dat.bodies.joint_positions(j,:);
        end
    end
end
end