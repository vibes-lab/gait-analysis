function[x_fit,y_fit, E_fit] = localize_ExpFit(E,x_sens,y_sens)
%{
    Localize footsteps using an energy decay approach

    This function uses a nonlinear least square regression to find the
    locations of footsteps. It assumes that footstep energy decays as its 
    vibration waves travel across the floor. 

    Inputs:
    * E - footstep RMS matrix
    * x_sens, y_sens - sensor coordinates

    Outputs:
    * x_fit, y_fit, E_fit - least squares estimates for the x and y
    positions of detected footsteps along with the estimated energy for
    each footstep.
%}

% Obtain initial location estimate
[x_start,y_start] = localize_WeightedSens(E,x_sens,y_sens, 0); 
N = size(E,1); % Num of steps

ydata = zeros(N,size(E,2));
xr_yr_sens = zeros(N,2);

% Get reference sensors and initial solutions
for i = 1: N
    Qi = E(i,:);
    
    Qr = max(E(i,:));
    
    loc = find(E(i,:) == Qr); 
    xr_yr_sens(i,:) =[ x_sens(loc), y_sens(loc)]; % Location of the reference sensors
    
    ydata(i,:) = Qi./Qr; % Initial solutions
    
end
  
x_use = [-0.5;x_start;y_start]; % Vector with beta, and initial footstep positions

% Energy dissipation equation
fun = @(x) exp(x(1)*((sqrt((x(2:1+N)-x_sens).^2 + (x(2+N:1+2*N)-y_sens).^2)) ... 
    - sqrt((x(2:1+N) - xr_yr_sens(:,1)).^2 + (x(2+N:1+2*N)- xr_yr_sens(:,2)).^2))) - ydata; 

lb = [  -0.75;  0*ones(N,1);    -3.5*ones(N,1)]; % Lower par. boundaries 
ub = [      0;  1.5*ones(N,1);  10.5*ones(N,1)]; % Upper par. boundaries 

opts = optimoptions('lsqnonlin','MaxFunctionEvaluations',100000,...
    'MaxIterations',100000, 'Algorithm', 'trust-region-reflective');

x = lsqnonlin(fun, x_use, lb, ub, opts);

x_fit = x(2:1+N);
y_fit = x(2+N:1+2*N);
E_fit = fun(x); % Regression line

%% Plots 
    x_true_pos = 0.75 * ones(20,1);
    y_true_pos = -2.8:0.7:10.5;
    y_true_pos = y_true_pos';
    num_steps = 1:20;
    
    % Errors in x
%     figure; hold on; grid on;
%     plot(num_steps, x_true_pos, 'k.', 'MarkerSize', 40)
%     plot(1:length(x_fit), x_fit, 'b.', 'MarkerSize', 40)
%     legend('True Pos.', 'Estimated Pos.','FontSize', 20)
%     set(gca, 'FontSize', 18)
%     xlabel('Step Number', 'FontSize', 20)
%     ylabel('x-coordinate (m)', 'FontSize', 20)
    
    % Erros in y
    figure; hold on; grid on;
    plot(num_steps, y_true_pos, 'k-o', 'LineWidth', 2, 'MarkerSize', 10)
    plot(1:length(y_fit), y_fit, 'b-o', 'LineWidth', 2, 'MarkerSize', 10)
    legend('True Pos.', 'Estimated Pos.','FontSize', 20)
    set(gca, 'FontSize', 18)
    xlabel('Step Number', 'FontSize', 20)
    ylabel('y-coordinate (m)', 'FontSize', 20)
    
%     % Total plotx

%     figure; hold on; grid on;
%     plot(x_true_pos, y_true_pos, 'k.', 'MarkerSize', 40)
%     plot(x_fit, y_fit, 'b.', 'MarkerSize', 40)
%     legend('True Pos.', 'Estimated Pos.','FontSize', 20)
%     set(gca, 'FontSize', 18)
%     xlabel('x-coordinate (m)', 'FontSize', 20)
%     ylabel('y-coordinate (m)', 'FontSize', 20)
    
   %% Calculate Errors
   % Total Walk
%    % Squared Errors
%    error_x = (x_true_pos - x_fit).^2;
%    error_y = (y_true_pos - y_fit).^2;
%    
%    % Mean Squared Errors
%    mean_x = mean(error_x);
%    mean_y = mean(error_y);
%    
%    % RMSE
%    RMSE_x = sqrt(mean_x)
%    RMSE_y = sqrt(mean_y)
%    
%    % Convex-Hull
%    x_true_convex = x_true_pos(5:15);
%    y_true_convex = y_true_pos(5:15);
%    
%    % Squared Errors
%    convex_error_x = (x_true_convex - x_fit(5:15)).^2;
%    convex_error_y = (y_true_convex - y_fit(5:15)).^2;
%    
%    % Mean Squared Errors
%    convex_mean_x = mean(convex_error_x);
%    convex_mean_y = mean(convex_error_y);
%    
%    % RMSE
%    convex_RMSE_x = sqrt(convex_mean_x)
%    convex_RMSE_y = sqrt(convex_mean_y)
   
   

end