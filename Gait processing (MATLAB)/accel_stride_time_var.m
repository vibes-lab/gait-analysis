function [total_str_time_CV, left_CV, right_CV] = ...
    accel_stride_time_var(inds, t)
%{
    Get stride time variability from the accel. data
%}

step_num = length(inds); % Num of steps

% Get left and right steps - first step was with the left leg.
if mod(step_num,2) ~= 0  % Obtains the last step leg
    left_leg_inds = inds(1:2:end);
    right_leg_inds = inds(2:2:end-1);
else
    left_leg_inds = inds(1:2:end-1);
    right_leg_inds = inds(2:2:end);
end

left_leg_time = t(left_leg_inds);  % Time stamps
right_leg_time = t(right_leg_inds);


left_str_time = diff(left_leg_time); % Stride time
right_str_time = diff(right_leg_time);

%% Get CV
% Left Leg
left_leg_mean = mean(left_str_time);  % Mean left stride time                
left_STD = std(left_str_time);  

left_CV = (left_STD/left_leg_mean) * 100; % Coefficient of variability (CV)

% Right Leg
right_leg_mean = mean(right_str_time) ;   % Mean right stride time           
right_STD = std(right_str_time);

right_CV = (right_STD/right_leg_mean) * 100; % Right CV

% Overall
combined_strides = [left_str_time; right_str_time];
accel_str_length = mean(combined_strides);
total_std = std(combined_strides);

total_str_time_CV = (total_std / accel_str_length) * 100;
end