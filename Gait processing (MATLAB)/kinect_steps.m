function [left_HS, left_TO, right_HS, right_TO] = ...
    kinect_steps(left_ankle, right_ankle, pelvis, kinect_time, fs, walking_speed, trial_num)
%{
     Extract footsteps from Kinect data according to the algorithm given by
     Zeni et. al. - https://pubmed.ncbi.nlm.nih.gov/17723303/

     HS - heelstrike
     TO - toe-off
%}
excludeLastStep = false;

% Extracting footsteps
left_steps = left_ankle(:,3) - pelvis(:,3);
I_lv = left_steps ~= 0; %valid indices for the left foot
right_steps = right_ankle(:,3) - pelvis(:,3);
I_rv = right_steps ~= 0; %valid indices for the right foot

 if walking_speed == "Regular" % For trials at regular walking speeds
    [~, left_HS] = findpeaks(left_steps(I_lv), kinect_time(I_lv),...
        'MinPeakHeight', mean(left_steps(I_lv)) + 0.5*std(left_steps(I_lv)),...
        'MinPeakDist', .4);
    left_HS = value2Index(left_HS,kinect_time); %get the indeces of each peak

    [~, left_TO] = findpeaks(-left_steps(I_lv),kinect_time(I_lv),...
        'MinPeakHeight', -mean(left_steps(I_lv)),...
        'MinPeakDist', .4);
    left_TO = value2Index(left_TO,kinect_time); %get the indeces of each peak

    [~, right_HS] = findpeaks(right_steps(I_rv), kinect_time(I_rv),...
        'MinPeakHeight', mean(right_steps(I_rv)) + 0.5*std(right_steps(I_rv)), ...
        'MinPeakDist', .4);
    right_HS = value2Index(right_HS,kinect_time); %get the indeces of each peak

    [~, right_TO] = findpeaks(-right_steps(I_rv), kinect_time(I_rv),...
        'MinPeakHeight', -mean(right_steps(I_rv)), ...
        'MinPeakDist', .4);
    right_TO = value2Index(right_TO,kinect_time); %get the indeces of each peak

 elseif walking_speed == "Fast"
    [~, left_HS] = findpeaks(left_steps(I_lv), kinect_time(I_lv),...
        'MinPeakHeight', mean(left_steps(I_lv)) + 1*std(left_steps(I_lv)),...
        'MinPeakDist', .33);
    left_HS = value2Index(left_HS,kinect_time); %get the indeces of each peak

    [~, left_TO] = findpeaks(-left_steps(I_lv),kinect_time(I_lv),...
        'MinPeakHeight', -mean(left_steps(I_lv)),...
        'MinPeakDist', .33);
    left_TO = value2Index(left_TO,kinect_time); %get the indeces of each peak

    [~, right_HS] = findpeaks(right_steps(I_rv), kinect_time(I_rv),...
        'MinPeakHeight', mean(right_steps(I_rv)) + 1*std(right_steps(I_rv)), ...
        'MinPeakDist', .33);
    right_HS = value2Index(right_HS,kinect_time); %get the indeces of each peak

    [~, right_TO] = findpeaks(-right_steps(I_rv), kinect_time(I_rv),... 
        'MinPeakHeight', -mean(right_steps(I_rv)), ...
        'MinPeakDist', .33);
    right_TO = value2Index(right_TO,kinect_time); %get the indeces of each peak        
 end


if mod(trial_num, 2) == 0 && excludeLastStep % Even walking trials (walking towards the kinect)
 % Exclude last detected footsteps for accuracy improvement
 finish = round(left_HS(1));  

 del_left_HS = left_HS < finish;
 del_left_TO = left_TO < finish;
 del_right_HS = right_HS < finish;
 del_right_TO = right_TO < finish;

 left_HS(del_left_HS) = [];
 left_TO (del_left_TO) = [];
 right_HS(del_right_HS) = [];
 right_TO(del_right_TO) = [];

else
 if walking_speed == "Regular"
     finish = round(left_HS(1) + fs * 3);  

     del_left_HS = left_HS > finish;
     del_left_TO = left_TO > finish;
     del_right_HS = right_HS > finish;
     del_right_TO = right_TO > finish;

     left_HS(del_left_HS) = [];
     left_TO (del_left_TO) = [];
     right_HS(del_right_HS) = [];
     right_TO(del_right_TO) = [];

 else
     finish = round(left_HS(1) + fs * 2);  

     del_left_HS = left_HS > finish;
     del_left_TO = left_TO > finish;
     del_right_HS = right_HS > finish;
     del_right_TO = right_TO > finish;

     left_HS(del_left_HS) = [];
     left_TO (del_left_TO) = [];
     right_HS(del_right_HS) = [];
     right_TO(del_right_TO) = [];
 end
end

% Switch vectos if person is walking towards the Kinect (to make it
% consistent to trials away from the Kinect)
if mod(trial_num, 2) == 0
    temp1 = left_HS;
    left_HS = left_TO;
    left_TO = temp1;

    temp2 = right_HS;
    right_HS = right_TO;
    right_TO = temp2;
end
end