function [walking_dist, walking_time, inst_gs, avg_gs,gait_dir] = ...
    kinect_walking_distance(pelvis, time_in_sec,plot_on)
% Get the total tracking distance from the Kinect by following the "pelvis"
% joint

pelvis_mag = (sum(pelvis.^2,2)).^0.5; % 1D magnitude of pelvis position (mm)
I_val = pelvis_mag ~= 0; %valid indices (nonzero)
inst_gs_val =[0; diff(pelvis_mag(I_val)/1e3)./diff(time_in_sec(I_val))]; %instantaneous gait speed in m/s
inst_gs = zeros(length(time_in_sec),1);
inst_gs(I_val) = inst_gs_val;
avg_gs = mean(inst_gs_val);

if avg_gs < 0 % Negative gait speed (moving towards the Kinect)
    gait_dir = 'away';
else 
    gait_dir = 'toward';
end

% Make sure noise doesn't interfere with measurements
minimum_ind = I_val(1);
maximum_ind = I_val(end);

% Calculate true walking distance according to the Pythagorean theorem
walking_dist = abs(pelvis_mag(minimum_ind) - pelvis_mag(maximum_ind)) / 1e3;
walking_time = abs(time_in_sec(minimum_ind) - time_in_sec(maximum_ind));

if plot_on
    figure; hold on; grid on;
    subplot(2,1,1)
    plot(time_in_sec, pelvis_mag/1000, 'linewidth', 2.5)
    title('Pelvis position', 'FontSize', 18)
    xlabel('Time (s)', 'FontSize', 18)
    ylabel('Position (m)', 'FontSize', 18)
    ax = gca;
    ax.FontSize = 16;
    subplot(2,1,2)
    plot(time_in_sec, inst_gs, 'linewidth', 2.5)
    title('Gait Speed', 'FontSize', 18)
    xlabel('Time (s)', 'FontSize', 18)
    ylabel('Gait Speed (m/s)', 'FontSize', 18)
    ax = gca;
    ax.FontSize = 16;
end
end
