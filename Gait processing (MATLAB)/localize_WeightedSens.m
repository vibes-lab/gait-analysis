function[x_fit,y_fit] = localize_WeightedSens(E,x_sens,y_sens)
%{
    Get a first estimate of footstep location
    
    Estimation is obtained by multiplying the energy vector with the sensor
    coordinates. 

    Inputs:
    * E - footsteps RMS matrix
    * x_sens, y_sens - vectors with the sensor coordinates

    Outputs:
    * x_fit, y_fit - first footstep localization estimate.
  
%}

x_fit = sum(E.*x_sens,2)./sum(E,2);
y_fit = sum(E.*y_sens,2)./sum(E,2);

% Plot rough estimate
% figure; 
% scatter(x_fit,y_fit,70,1:length(x_fit),'filled'); hold on;
% % x_smooth = smooth(x_fit,2);  % Smoothed event path
% % y_smooth = smooth(y_fit,2);
% % plot(x_smooth(2:end-1),y_smooth(2:end-1),'b--','LineWidth',2); 
% plot(x_sens,y_sens,'ko','LineWidth',2); % Plots sensor locations
% 
% % Plot start-end lines
% plot([0 1.5], [-3.5 -3.5], '--k', 'linew', 2)
% plot([0 1.5], [10.5 10.5], '--k', 'linew', 2)
% 
% % Plot walls
% plot([-0.5 -0.5],[-4 12],'k','LineWidth',2);
% plot([2 2],[-4 12],'k','LineWidth',2);      
% 
% legend('Estimated Event Locations','Sensor Locations',...
%     'Start / End Pos.','location','northwest','FontSize',20);
% c=colorbar('Ticks',[1,length(x_fit)],'TickLabels',{'First','Last'},...
%     'FontSize',20);
% c.Label.String = 'Event Order';
% c.Label.FontSize = 20;
% set(gca, 'FontSize',18);
% xlabel('x-coordinate (m)','FontSize',20);
% ylabel('y-coordinate (m)','FontSize',20);
% xlim([-2 3.5]); 
% ylim([-5 15]);
% end