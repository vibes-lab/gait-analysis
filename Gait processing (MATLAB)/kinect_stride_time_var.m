function [kinect_left_str_CV, kinect_right_str_CV, tot_kinect_str_time_CV] = ...
    kinect_stride_time_var(left_HS, right_HS, kinect_time)
%{
    Get step time variability from kinect data
%}

% Get stride times
left_leg_time = kinect_time(left_HS);
right_leg_time = kinect_time(right_HS);

% Left leg
left_stride_time = diff(left_leg_time);
left_leg_mean = mean(left_stride_time);
left_STD = std(left_stride_time);

% Right leg
right_stride_time = diff(right_leg_time);
right_leg_mean = mean(right_stride_time);
right_STD = std(right_stride_time);

%% Get CV's
kinect_left_str_CV = (left_STD / left_leg_mean) * 100;
kinect_right_str_CV = (right_STD / right_leg_mean) * 100;

% Overall CV
strides = sort([left_stride_time; right_stride_time]);
stride_length = mean(strides);
stride_length_std = std(strides);
tot_kinect_str_time_CV = (stride_length_std / stride_length) * 100;

end
    