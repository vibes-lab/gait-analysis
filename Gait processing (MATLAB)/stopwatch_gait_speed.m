function stopwatch_gs = stopwatch_gait_speed(distance,stopwatch_times)
%% Stopwatch gait speed

stopwatch_gs = distance / sum(stopwatch_times);
end