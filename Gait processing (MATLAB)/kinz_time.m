function [time_in_sec, fs] = kinz_time(kinz_data)
% Get time vector from Kinect

frames_num = length(kinz_data);
time_in_sec = zeros(frames_num, 1);
time0 = double(kinz_data(1).timeStamps); % time zero for the acquisition

for i = 1:frames_num
    time_dbl = double(kinz_data(i).timeStamps) -time0; %relative time (double precision)
    time_in_sec(i) = time_dbl/1e9; % Convert time 
    % to seconds.
end

fs = 1/mode(diff(time_in_sec)); % Sampling rate
end