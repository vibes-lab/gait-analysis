function [joint_positions, hasBodies] = kinz_joints_positions(kinz_data)
% Extract the position of every tracked joing and save them in a matrix.

% Define joints
joints = {'PELVIS';'SPINE_NAVEL';'SPINE_CHEST';'NECK';'CLAVICLE_LEFT';...
    'SHOULDER_LEFT';'ELBOW_LEFT';'WRIST_LEFT';'HAND_LEFT';'HANDTIP_LEFT';...
    'THUMB_LEFT';'CLAVICLE_RIGHT';'SHOULDER_RIGHT';'ELBOW_RIGHT';...
    'WRIST_RIGHT';'HAND_RIGHT';'HANDTIP_RIGHT';'THUMB_RIGHT';...
    'HIP_LEFT';'KNEE_LEFT';'ANKLE_LEFT';'FOOT_LEFT';'HIP_RIGHT';...
    'KNEE_RIGHT';'ANKLE_RIGHT';'FOOT_RIGHT';'HEAD';'NOSE';'EYE_LEFT';...
    'EAR_LEFT';'EYE_RIGHT';'EAR_RIGHT'};

frames_num = length(kinz_data); % Total number of frames 
joints_num = length(joints); % Total number of joints

joint_positions = zeros(frames_num, 3, joints_num); % 3 columns because 
% joint positions are given in xyz.
hasBodies = false;
for i = 1:frames_num
    for j = 1:joints_num
        if isempty(kinz_data(i).bodies)
            joint_positions(i, :, j) = 0;
        else
            joint_positions(i,:,j) = kinz_data(i).bodies(1).Position3d(:,j);
            hasBodies = true;
        end
    end
end
end