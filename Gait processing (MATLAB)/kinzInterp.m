function kinzDataInt = kinzInterp(kinzData,frameI)
% Interpolates the joint Position data in kinzData at frame index frameI
% using values from the adjacent frames.

%% Check that adjacent points are defined
if kinzData(frameI-1).numbodies == 0 || kinzData(frameI+1).numbodies == 0
    error('Adjacent points are not defined, cannot perform interpolation.')
end

%% Gather the interpolation points
[time_sec, ~] = kinz_time(kinzData);
t_b = time_sec(frameI-1);
t_c = time_sec(frameI);
t_a = time_sec(frameI+1);
pos3d_b = kinzData(frameI-1).bodies.Position3d;
pos3d_a = kinzData(frameI+1).bodies.Position3d;
pos2d_b = kinzData(frameI-1).bodies.Position2d_rgb;
pos2d_a = kinzData(frameI+1).bodies.Position2d_rgb;
orient_b = kinzData(frameI-1).bodies.Orientation;
orient_a = kinzData(frameI+1).bodies.Orientation;
conf_b = kinzData(frameI-1).bodies.Confidence;
conf_a = kinzData(frameI+1).bodies.Confidence;

%% Perform the interpolations
pos3d_c = pos3d_b + (pos3d_a-pos3d_b).*(t_c-t_b)./(t_a-t_b);
pos2d_c = pos2d_b + (pos2d_a-pos2d_b).*(t_c-t_b)./(t_a-t_b);
orient_c = orient_b + (orient_a-orient_b).*(t_c-t_b)./(t_a-t_b);
conf_c = conf_b + (conf_a-conf_b).*(t_c-t_b)./(t_a-t_b);

%% Update kinzData
kinzDataInt = kinzData;
kinzDataInt(frameI).bodies(1).Position3d = pos3d_c;
kinzDataInt(frameI).bodies(1).Position2d_rgb = pos2d_c;
kinzDataInt(frameI).bodies(1).Orientation = orient_c;
kinzDataInt(frameI).bodies(1).Confidence = conf_c;
kinzDataInt(frameI).numbodies = 1;
