function [event_E, event_inds, SNR,warn] = ...
    detect_events_SNR(x, window_size, peak_dist, fs,plot_on)
%{
    Detect footstep according to SNR
    
    This function measures the RMS of the entire signal and then looks for
    peaks that would indicate an event (such as footstep). Footsteps are
    classified by the ratio of a particular RMS of a window compared with 
    the previous one. If this ratio (named signal-to-noise ratio) is above
    a threashold, the event is classified as a footstep.

    Inputs:
    * Vibration signal from all sensors.
    * Window size to calculate RMS (in samples).
    * Minimum peak distance between footsteps (in samples).
    * Sample rate.

    Output:
    * E - energy matrix containing the energy in each detected footstep
    * event_inds - indices of detected footsteps
    * SNR - normalized Signal-to-Noise ratio signal 
%}

warn = false;
%% RMS
E = movmean(x.^2,[0,window_size]); % Computes the RMS of the signal
all_inds = 1:window_size:size(E,1); % Defines where each window starts
E = E(all_inds,:); % Gets the RMS of each window

%% SNR
SNR = E./median(E); % Computes SNR 

% Plot unormalized SNR
% figure;
% plot(SNR);
% title("SNR")

SNR = max(SNR,[],2); % Gets the max SNR of each sensor for each window

sd = .1; % Threashold of detection

%% Find Footsteps

% Find all footsteps that differ from the mean according to sd.
[~,event_SNR_inds] = findpeaks(SNR, 'MinPeakHeight', ...
    mean(SNR + std(SNR)*sd), 'MinPeakDistance', floor(peak_dist/window_size));

event_inds = all_inds(event_SNR_inds); % Finds the location of each detected footstep

%% Check for issues
% Find peaks without minimum difference, if data is good there should not
% be a large difference
[~,check_SNR_inds] = findpeaks(SNR, 'MinPeakHeight', ...
    mean(SNR + std(SNR)*sd*5));

if length(check_SNR_inds) > 1.3*length(event_SNR_inds)
    warn = true;
%     figure
%     subplot(311)
%     findpeaks(SNR, 'MinPeakHeight', ...
%     mean(SNR + std(SNR)*sd), 'MinPeakDistance', floor(peak_dist/window_size));
%     subplot(312)
%     findpeaks(SNR, 'MinPeakHeight', ...
%     mean(SNR + std(SNR)*sd*5));
%     subplot(313)
%     plot(x)
    warning('Too many peaks: footstep detection may be faulty.')
end

%% Calculate footstep power
% Redifine E
E = movmean(x.^2, [0, window_size]);

% Get the average power in each footstep
event_E = zeros(length(event_inds), size(E,2));

for r = 1:length(event_inds)
    % Define start and peak points of the wave
    k0 = event_inds(r) - (4 * window_size);
    kp = event_inds(r) + (2 * window_size);
    
    if k0 < 1 
        k0 = 1;
    end
    
    if kp > size(E,1)
        kp = size(E,1);
    end
    % Calculate average footstep power
    for c = 1:size(E,2)
        event_E(r,c) = fs*sum(E(k0:kp, c)); %RS: change from mean to fs*sum
    end
end
% Remove outliers
[event_E, event_inds] = remove_outliers(event_E, event_inds, fs, 0.7);


%% Plot detected footsteps on top of SNR
if plot_on
    figure; hold on;
    findpeaks(SNR, 'MinPeakHeight', ...
        mean(SNR + std(SNR))*sd, 'MinPeakDistance', floor(peak_dist/window_size))
     xlabel('Sample', 'FontSize', 14)
    ylabel('Avg. Acceleration RMS (m/s^2)', 'FontSize', 14)
    legend('Acceleration', 'Detected Footsteps', 'FontSize', 14)
    title('Footstep Detection')
end

end