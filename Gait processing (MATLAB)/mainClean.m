% Clean raw data

%% Get the folder structure
patientDir = '/Volumes/GoogleDrive/My Drive/CEE/Code_Repositories/gait-analysis/Patient Data/';
cd(patientDir);
D = dir;
D = D(4:end);
nPatients = length(D);

%% Fetch data from each folder and clean
offset = 0.15; %approximate offset between kinect and accel for better alignment
for k = 1:nPatients
    cd([patientDir '/' D(k).name]);
    files = dir;
    files = files(3:end);
    nFiles = length(files);
    for f = 1:nFiles
        load(files(f).name);
        
        if ~strcmp(files(f).name(1:3),'Cln') && ~strcmp(files(f).name(1:4),'Fail')
            % Clean Kinect data
            [kinzData,tRange] = cleanKinzData(kinzData);
            [accelData] = cleanAccelData(accelData);

            if isempty(kinzData) && isempty(accelData)
                disp(['Failed Clean on: ' files(f).name])
                save(['FailAK-' files(f).name],"kinzData","accelData","kinzRef",...
                    "stopwatch","patientID","trial_num","notes","walking_speed",...
                    "time");
            elseif isempty(kinzData)
                disp(['Failed Kinz Clean on: ' files(f).name])
                save(['FailK-' files(f).name],"kinzData","accelData","kinzRef",...
                    "stopwatch","patientID","trial_num","notes","walking_speed",...
                    "time");
            elseif isempty(accelData)
                disp(['Failed Accel Clean on: ' files(f).name])
                save(['FailA-' files(f).name],"kinzData","accelData","kinzRef",...
                    "stopwatch","patientID","trial_num","notes","walking_speed",...
                    "time");
            else
                %Shift accel data times to match clean kinect data
                accelData.timeStamps = accelData.timeStamps-tRange(1)+offset;
        
                save(['Cln-' files(f).name],"kinzData","accelData","kinzRef",...
                    "stopwatch","patientID","trial_num","notes","walking_speed",...
                    "time");
            end
        end
    end
end