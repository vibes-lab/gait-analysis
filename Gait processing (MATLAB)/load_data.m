function [x,t,fs, stopwatch] = load_data(filename)
%{ 
     load_data() loads the file with acceleration data,
     normalizes the data by the sensors sensitivities, and 
     returns:

     - a MxN matrix X where each column has acc. data from a sensor.

     - a time vector t.

     - a sampling rate vector fs.

     - the total walking time measured by the stopwatch
%}

load(filename);
%% Sensitivities 

s1 = 0.988;
s2 = 1.000;
s3 = 1.012;
s4 = 0.998;
s5 = 1.000;
s6 = 0.992;
s7 = 0.996;

x = [sensor_1/s1, sensor_2/s2, sensor_3/s3, sensor_4/s4, sensor_5/s5, sensor_6/s6, sensor_7/s7];

t = time;

fs = 1/mode(diff(time));

stopwatch = section_times;
end