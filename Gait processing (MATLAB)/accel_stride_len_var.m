function [accel_str_length, accel_str_CV, accel_left_str_CV, ...
    accel_right_str_CV] = accel_stride_len_var(y_fit, gait_dir)
% Get stride lenth variability for footsteps localized inside the convex-hull.

% Find first and last steps inside convex-hull
if strcmp(gait_dir,'away')
    first_steps = find(y_fit < 0.01);
    last_steps = find(y_fit > 6.99);

    if isempty(first_steps)
        first_steps = 1;
    end

    first_ind = first_steps(end);
    last_ind = last_steps(1);
else
    first_steps = find(y_fit > 6.99);
    last_steps = find(y_fit < 0.01);

    if isempty(last_steps)
        last_steps = length(y_fit);
    end 

    if isempty(first_steps)
        first_steps = 1;
    end

    first_ind = first_steps(end);
    last_ind = last_steps(1);
end

% Adjust vectors according to detected steps
y_fit = y_fit(first_ind:last_ind);

%% Get left and right steps
step_num = length(y_fit); % Num of steps

% Get left and right steps - first step was with the left leg.
if mod(step_num,2) ~= 0  % Obtains the last step leg
    left_steps = y_fit(1:2:end);
    right_steps = y_fit(2:2:end-1);
else
    left_steps = y_fit(1:2:end-1);
    right_steps = y_fit(2:2:end);
end

%% Get Stride lengths
left_stride = abs(diff(left_steps));
right_stride = abs(diff(right_steps));

%% Get CV's
% Left steps
left_mean = mean(left_stride);
left_std = std(left_stride);

accel_left_str_CV = (left_std / left_mean) * 100;

% Right steps
right_mean = mean(right_stride);
right_std = std(right_stride);

accel_right_str_CV = (right_std / right_mean) * 100;

%% Get overall CV
combined_strides = [left_stride; right_stride];
accel_str_length = mean(combined_strides);
total_std = std(combined_strides);

accel_str_CV = (total_std / accel_str_length) * 100;
end