% Aggregate patient data

%% Get the folder structure
patientDir = '/Volumes/GoogleDrive/My Drive/CEE/Code_Repositories/gait-analysis/Patient Data/';
cd(patientDir);
D = dir;
D = D(4:end);
nPatients = length(D);

%% Initalize aggregator arrays
%accel=accelerometer, kin=kinect, sw=stopwatch
%gs=gaitspeed,cad=cadence
accel_gs_reg = zeros(nPatients,1);
accel_gs_fast = zeros(nPatients,1);
kin_gs_reg = zeros(nPatients,1);
kin_gs_fast = zeros(nPatients,1);
sw_gs_reg = zeros(nPatients,1);
sw_gs_fast = zeros(nPatients,1);
accel_cad_reg = zeros(nPatients,1);
accel_cad_fast = zeros(nPatients,1);
kin_cad_reg = zeros(nPatients,1);
kin_cad_fast = zeros(nPatients,1);

%% Sensor settings
accelSensitivity = [1.001,1.000,1.009,1.058,1.003,1.038,1.042]; % Accelerometer Sensitivity
sensorIndex = [1,2,3,5,6,7,4]; % Indices of physical sensors
x_sens = [0,    0,    0, 1.50, 1.50, 1.50];  % x - coordinates for s1, s2, ..., s5,s6
y_sens = [0, 3.50, 7.00, 7.00, 3.50,    0];  % y - coordinates for s1, s2, ..., s5,s6

%% Fetch patient data from each folder
for k = 1:nPatients
    cd([patientDir '/' D(k).name]);
    files = dir;
    files = files(3:end);
    nFiles = length(files);
    pat_warn = zeros(nPatients,1); %possible fault in footstep detection
    pat_gs = zeros(nFiles,3); %patient gait speeds, 1=accel,2=kinect, 3=sw
    pat_cad = zeros(nFiles,2);%patient cadences, 1=accel,2=kinect
    pat_gs_fast = zeros(nFiles,3); %patient gait speeds, 1=accel,2=kinect, 3=sw
    pat_cad_fast = zeros(nFiles,2);%patient cadences, 1=accel,2=kinect
    reg_ct = 0;
    fast_ct = 0;
    for f = 1:nFiles
        load(files(f).name);
        files(f).name
        if strcmp(files(f).name(1:3),'Cln')
            % =============== Body Tracking ==========================
            
            % If any body tracking option is selected, run the basic
            % processing operations
            % Time vector
            [time_sec, kinect_fs] = kinz_time(kinzData);

            % Joints position
            [joint_positions, hasBodies] = kinz_joints_positions(kinzData);

            if hasBodies
                % Get ankles, feet, and pelvis positions
                [left_ankle, right_ankle, left_foot, right_foot, pelvis, left_hip, right_hip] = ...
                    kinect_leg_positions(joint_positions);

                % Extract footsteps
                [left_HS, left_TO, right_HS, right_TO] =...
                    kinect_steps(left_ankle, right_ankle, pelvis, time_sec,kinect_fs, ...
                    walking_speed, trial_num);
                
                
                % Extract walking distance
                [walking_dist, walking_time,inst_gs, avg_gs,gait_dir] = ...
                    kinect_walking_distance(pelvis, time_sec,0);

            else
                warning('No bodies found in data.')
            end

            % =============== Footstep Detection =========================
            % Define sensor sensitivities (change manually)
            sensitivity = accelSensitivity;

            % Rename variables
            x = accelData.timeSeries; %accelerometer data in volts
            sens_mat = repmat(sensitivity,length(x),1); %sensitivity matrix
            X = x./sens_mat; %Acceleration in g's;
            t = accelData.timeStamps;
            fs = accelData.fs;

            % Detrend data
            X = detrend_movmean(X,fs); % Detrends X with a moving average of window size fs

            % Align Signals
            X = align_peaks(X, fs, accelData.deadChannels);

            % Detect Events
            footstep_dur = 0.02; % in secs
            window_size = round(footstep_dur * fs);
            peak_distance = 0.3 * fs;

            [E,inds, SNR, warn] = detect_events_SNR(X, window_size, peak_distance, fs,0); 
            
            %Cadence via detected events
            [accel_cad, kinect_cad] = cadence(inds, t, left_HS, right_HS, time_sec);
            
            if ~warn
                %Localization is needed for these two analyses
                % Least Squares Localization
                [x_fit,y_fit] = localize_footsteps(E,inds,x_sens,y_sens,0);
    
                % Step length  via footstep tracking
                [accel_step_length, accel_step_CV] = ...
                    accel_step_var(y_fit, trial_num);
                %Gait speed via footstep tracking
                [step_length_gs, loc_gs, mean_loc_gs] = ...
                    accel_gait_speed(E,inds,t,accel_step_length,y_fit);
            else
                disp(['Event Detection warning on: ' files(f).name])
                mean_loc_gs = nan;
                accel_cad = nan;
                pat_warn(k) = 1;
            end

            % ================= Stopwatch ======================
            distance = 7; %meters
            sw_gs = distance / sum(stopwatch);

            % ================= Cadence ========================
                        
            % ================= Save results ===================
            
            
            if strcmp(walking_speed,'Regular')
                reg_ct = reg_ct+1;
                pat_gs(reg_ct,2) = abs(avg_gs);
                pat_gs(reg_ct,1) = mean_loc_gs;
                pat_cad(reg_ct,1) = accel_cad;
                pat_cad(reg_ct,2) = kinect_cad;
                pat_gs(reg_ct,3) = sw_gs;
            else
                fast_ct = fast_ct+1;
                pat_gs_fast(fast_ct,2) = abs(avg_gs);
                pat_gs_fast(fast_ct,1) = mean_loc_gs;
                pat_cad_fast(fast_ct,1) = accel_cad;
                pat_cad_fast(fast_ct,2) = kinect_cad;
                pat_gs_fast(fast_ct,3) = sw_gs;
            end
        end
    end
    pat_gs = pat_gs(1:reg_ct,:);
    pat_cad = pat_cad(1:reg_ct,:);
    pat_gs_fast = pat_gs_fast(1:reg_ct,:);
    pat_cad_fast = pat_cad_fast(1:reg_ct,:);
    
    accel_gs_reg(k) = mean(pat_gs(:,1),'omitnan');
    accel_gs_fast(k) = mean(pat_gs_fast(:,1),'omitnan');
    kin_gs_reg(k) = mean(pat_gs(:,2),'omitnan');
    kin_gs_fast(k) = mean(pat_gs_fast(:,2),'omitnan');
    sw_gs_reg(k) = mean(pat_gs(:,3),'omitnan');
    sw_gs_fast(k) = mean(pat_gs_fast(:,3),'omitnan');
    accel_cad_reg(k) = mean(pat_cad(:,1),'omitnan');
    accel_cad_fast(k) = mean(pat_cad_fast(:,1),'omitnan');
    kin_cad_reg(k) = mean(pat_cad(:,2),'omitnan');
    kin_cad_fast(k) = mean(pat_cad_fast(:,2),'omitnan');
end

%% Save
save('/Volumes/GoogleDrive/My Drive/CEE/Code_Repositories/gait-analysis/Other Data/GaitSp_Cadence_Agreggation_all',...
    "accel_cad_fast","accel_cad_reg","pat_warn",...
    "kin_cad_fast","kin_cad_reg","sw_gs_fast","accel_cad_reg",...
    "sw_gs_reg","kin_gs_fast","kin_gs_reg","accel_gs_fast","accel_gs_reg")
