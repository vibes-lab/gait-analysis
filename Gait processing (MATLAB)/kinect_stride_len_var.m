function [stride_length, stride_length_CV, left_stride_CV, right_stride_CV] = ...
    kinect_stride_len_var(left_foot_HS, right_foot_HS, left_foot, right_foot)
%{
    Get the stride distance variability from each leg using the kinect
    
    A stride is the distance from two consecutives TO or HS from the same
    leg. The variability is measured as an average from both legs
    using the CV.
    
    Inputs: 
    * left_HS, right_HS - vectors of left and right heelstrikes.
    * left_TO, right_TO - vectors of left right heelstrikes.
    * left_foot, right_foot - vectors of the total walking data of the left
    and right feet tracked by the kinect.
    
    Outputs:
    * stride_length - mean stride length during the walk.
    * stride_length_CV - CV of stride length.
    * left_stride_length, right_stride_length - vector with the stride 
    lengths from the left and right feet.
%}
    
% Adjust Z distance
    N = length(left_foot);
    left_foot_adjusted = zeros(N,1);
    right_foot_adjusted = zeros(N,1);
    for i = 1:N
        left_foot_adjusted(i) = sqrt(left_foot(i,3)^2 - 0.75^2);
        right_foot_adjusted(i) = sqrt(right_foot(i,3)^2 - 0.75^2);
    end
    
    % Steps loc
    left_HS_loc = abs(left_foot_adjusted(left_foot_HS));
    right_HS_loc = abs(right_foot_adjusted(right_foot_HS));
    
    % Stride lengths in meters
    left_stride_length = abs(diff(left_HS_loc))./10^3;
    right_stride_length = abs(diff(right_HS_loc))./10^3;
    
    %% CV
    % Overall 
    strides = sort([left_stride_length; right_stride_length]);
    stride_length = mean(strides);
    stride_length_std = std(strides);
    stride_length_CV = (stride_length_std / stride_length) * 100;
    
    % Left leg
    left_mean = mean(left_stride_length);
    left_std = std(left_stride_length);
    left_stride_CV = (left_std/left_mean) * 100;
    
    % Right leg
    right_mean = mean(right_stride_length);
    right_std = std(right_stride_length);
    right_stride_CV = (right_std/right_mean) * 100;
    