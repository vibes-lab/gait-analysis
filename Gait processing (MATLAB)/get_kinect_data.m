function [kinect_data, trial_num, walking_speed, walking_weight, walker_name,...
    kinect_file] = get_kinect_data(filename)
%{
    This function extracts the data and the trial information from the Kinect.
    underscore, walker_name, trial_num, walking_speed, walking_weights,
    and kinect_file are used later for some analysis and for saving the results.
%}
    
all_files = dir("Data/" + filename);
underscore = strfind(all_files.name,'_');
walker_name = all_files.name(1:underscore(1) - 1);
trial_num = str2double(all_files.name(underscore(end) + 1));
walking_speed = all_files.name(underscore(1) + 1:underscore(2) - 1);
walking_weight = all_files.name(underscore(3) + 1:underscore(4) - 1);

kinect_file = all_files.name;    
str = fileread("Data/" + kinect_file); % Dedicated for reading files as text 
kinect_data = jsondecode(str); % Using the jsondecode function to parse JSON from string
end
