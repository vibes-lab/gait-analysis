function [step_length, step_length_CV, left_step_CV, right_step_CV] = ...
    kinect_step_var(left_HS, left_TO, right_HS, right_TO, left_foot, right_foot)
%{
    Get the step distance variability from each leg using the kinect
    
    A step is the distance from a HS to the TO of the other leg. For
    example, it's the distance between the left leg HS to the TO of the
    right leg. The variability is measured as an average from both legs
    using the CV.
    
    Inputs: 
    * left_HS, right_HS - vectors of left and right heelstrikes.
    * left_TO, right_TO - vectors of left right heelstrikes.
    * left_foot, right_foot - vectors of the total walking data of the left
    and right feet tracked by the kinect.
    
    Outputs:
    * step_length - mean step legth during the walk.
    * step_length_CV - CV of step length.
    * left_step_length, right_step_length - vector with the step lengths
    from the left and right feet.
%}
    
    % Adjust Z distance using the Pythagorean Theorem
    N = length(left_foot);
    left_foot_adjusted = zeros(N,1);
    right_foot_adjusted = zeros(N,1);
    for i = 1:N
        left_foot_adjusted(i) = sqrt(left_foot(i,3)^2 - 0.75^2);
        right_foot_adjusted(i) = sqrt(right_foot(i,3)^2 - 0.75^2);
    end
    
    % Steps locations for each leg
    left_HS_loc = abs(left_foot_adjusted(left_HS));
    left_TO_loc = abs(left_foot_adjusted(left_TO));
    right_HS_loc = abs(right_foot_adjusted(right_HS));
    right_TO_loc = abs(right_foot_adjusted(right_TO));
    
    % Left steps dist
    left_steps = sort([left_HS_loc; right_TO_loc]);
    vec_diff = abs(length(left_HS_loc) - length(right_TO_loc));
    for i = 1:2:length(left_steps)-vec_diff
        left_step_length(i) = abs(left_steps(i) - left_steps(i+1));
    end
    left_step_length = left_step_length(left_step_length ~= 0)'./10^3;
    
    % right steps dist
    right_steps = sort([right_HS_loc; left_TO_loc]);
    vec_diff = abs(length(right_HS_loc) - length(left_TO_loc));
    for i = 1:2:length(right_steps)-vec_diff
        right_step_length(i) = abs(right_steps(i) - right_steps(i+1));
    end
    right_step_length = right_step_length(right_step_length ~= 0)'./10^3;
    
    %% Get Step Length CV's
    % Left steps
    left_mean = mean(left_step_length);
    left_std = std(left_step_length);
    left_step_CV = (left_std / left_mean) * 100;
    
    % Right steps
    right_mean = mean(right_step_length);
    right_std = std(right_step_length);
    right_step_CV = (right_std / right_mean) * 100;
    
    %% Get Mean Step Length
    step_length = mean([left_step_length; right_step_length]);
    step_length_CV = (std([left_step_length; right_step_length]) / step_length) * 100;
end