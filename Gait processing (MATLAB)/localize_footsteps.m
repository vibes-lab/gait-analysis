function[x_fit,y_fit] = localize_footsteps(E,inds,x_sens,y_sens,plot_on)
%{
    Localize footsteps using an energy decay approach

    This function uses a nonlinear least square regression to find the
    locations of footsteps. It assumes that footstep energy decays as its 
    vibration waves travel across the floor. More information can be found
    here: https://journals.sagepub.com/doi/abs/10.1177/1077546319890520?journalCode=jvcb

    Inputs:
    * E - footstep RMS matrix
    * inds - footstep indexes 
    * x_sens, y_sens - sensor coordinates

    Outputs:
    * x_fit, y_fit - least squares estimates for the x and y
    positions of detected footsteps along with the estimated energy for
    each footstep.
%}

% Obtain initial location estimate
[x_start, y_start] = localize_WeightedSens(E, x_sens, y_sens); 
N = size(E,1); % Num of steps

% Init. estimated footstep locations
x_fit = zeros(N,1);
y_fit = zeros(N,1);

% Get sensor coordinates to be used in the algorithm
r_ix = x_sens;
r_iy = y_sens;

% Init. algorithm params.
lb = [-0.75,  0,  -3.5]; % Lower par. boundaries 
ub = [    0, 1.5, 10.5]; % Upper par. boundaries 

opts = optimoptions('lsqnonlin','MaxFunctionEvaluations',100000,...
    'MaxIterations',100000, 'Algorithm', 'trust-region-reflective');

% Get reference sensors and initial solutions
for i = 1: N
    Qi = E(i,:); % Average power at each sensor for the ith footstep
    
    % Choose reference sensor
    Qr = max(E(i,:));
    loc = find(E(i,:) == Qr); % Get reference sensor index
    % i has to be different from loc to produce N-1 equations
    Qi(loc) = [];
    r_ix(loc) = [];
    r_iy(loc) = [];
    
    r_R =[x_sens(loc), y_sens(loc)]; % Location of the reference sensors
    
    ydata = Qi./Qr; % Initial solutions
    
    x_use = [-0.5, x_start(i), y_start(i)]; % Vector with beta, and 
% initial footstep positions
    
    % Energy dissipation equation
    fun = @(x) exp(x(1)*((sqrt(((x(2)-r_ix).^2) + (x(3)-r_iy).^2)) ... 
    - sqrt(((x(2) - r_R(1,1)).^2) + (x(3)- r_R(1,2)).^2))) - ydata; 

    x = lsqnonlin(fun, x_use, lb, ub, opts); % Nonlinear solver
    
    x_fit(i) = x(2);
    y_fit(i) = x(3);
    
    % Reset sensor coordinates
    r_ix = x_sens;
    r_iy = y_sens;
    
end

if plot_on
% Plot localization results
figure; hold on;
% Plot detected footsteps
x_fixed = ones(length(y_fit),1)*0.75;
hull_indx = 2:(length(y_fit)-1);
scatter(x_fixed(hull_indx),y_fit(hull_indx),50,1:length(hull_indx),'filled'); hold on;

% Plots sensor locations
plot(x_sens,y_sens,'ko','LineWidth',2, 'MarkerSize', 10); 

% Plot start-end lines
plot([0 1.5], [-3.5 -3.5], '--k', 'linew', 3)
plot([0 1.5], [10.5 10.5], '--k', 'linew', 3)


% Plot walls
plot([-0.5 -0.5],[-4 12],'k','LineWidth',3);
plot([2 2],[-4 12],'k','LineWidth',3);

legend('Estimated Event Locations','Sensor Locations', 'Start / End Pos.',...
    'location','northwest','FontSize',14);
c=colorbar('Ticks',[1,length(inds)],'TickLabels',{'First','Last'},...
    'FontSize',20);
c.Label.String = 'Event Order';
c.Label.FontSize = 19;
set(gca, 'FontSize',12);
xlabel('x-coordinate (m)','FontSize',14);
ylabel('y-coordinate (m)','FontSize',14);
xlim([-2 3.5]); 
ylim([-5 15]);
end
end