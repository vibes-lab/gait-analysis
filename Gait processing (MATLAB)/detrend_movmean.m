function [x_detrend] = detrend_movmean(x_orig,window_size)
% Detrends data by removing a moving average

x_detrend = x_orig - movmean(x_orig,window_size);

end