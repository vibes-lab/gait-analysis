function [left_ankle, right_ankle, left_foot, right_foot, pelvis, left_hip, right_hip] = ...
         kinz_leg_positions(joint_positions)
% Extract the position of specific body parts

% Body parts ID's
left_ankle_id = 21;  % Kinect node indexes for each joint
right_ankle_id = 25;
left_foot_id = 22;
right_foot_id = 26;
pelvis_id = 1;
left_hip_id = 19;
right_hip_id = 23;

% Get positional data 
left_ankle_x = joint_positions(:,1,left_ankle_id);
left_ankle_y = joint_positions(:,2,left_ankle_id);
left_ankle_z = joint_positions(:,3,left_ankle_id);

left_ankle = [left_ankle_x, left_ankle_y, left_ankle_z];

right_ankle_x = joint_positions(:,1,right_ankle_id);
right_ankle_y = joint_positions(:,2,right_ankle_id);
right_ankle_z = joint_positions(:,3,right_ankle_id);

right_ankle = [right_ankle_x, right_ankle_y, right_ankle_z];

left_foot_x = joint_positions(:,1,left_foot_id);
left_foot_y = joint_positions(:,2,left_foot_id);
left_foot_z = joint_positions(:,3,left_foot_id);

left_foot = [left_foot_x, left_foot_y, left_foot_z];

right_foot_x = joint_positions(:,1,right_foot_id);
right_foot_y = joint_positions(:,2,right_foot_id);
right_foot_z = joint_positions(:,3,right_foot_id);

right_foot = [right_foot_x, right_foot_y, right_foot_z];

pelvis_x = joint_positions(:,1,pelvis_id);
pelvis_y = joint_positions(:,2,pelvis_id);
pelvis_z = joint_positions(:,3,pelvis_id);

pelvis = [pelvis_x, pelvis_y, pelvis_z];

left_hip_x = joint_positions(:,1,left_hip_id);
left_hip_y = joint_positions(:,2,left_hip_id);
left_hip_z = joint_positions(:,3,left_hip_id);

left_hip = [left_hip_x, left_hip_y, left_hip_z];

right_hip_x = joint_positions(:,1,right_hip_id);
right_hip_y = joint_positions(:,2,right_hip_id);
right_hip_z = joint_positions(:,3,right_hip_id);

right_hip = [right_hip_x, right_hip_y, right_hip_z];
 
end