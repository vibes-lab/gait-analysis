function [cleanData] = cleanAccelData(rawData)

cleanData = rawData;
%% Check number of samples
if rawData.nSamples == 0 || rawData.nSamples ~= size(rawData.timeSeries,1)
    %output empty data
    cleanData = [];
    disp('Accel Clean: Data acquisition fail.')
    return
end

%% Detrend
cleanData.timeSeries = detrend(cleanData.timeSeries,1);


%% High pass filter to remove drift
wc = 5;
[b,a] = butter(2,2*wc/rawData.fs,'high');
cleanData.timeSeries = filtfilt(b,a,cleanData.timeSeries);

%% Low pass filter to remove mechanical noise
wc = 250;
[b,a] = butter(2,2*wc/rawData.fs,'low');
cleanData.timeSeries = filtfilt(b,a,cleanData.timeSeries);

%% Clip spikes to 0.05 g
% a_max = max(max(abs(cleanData.timeSeries)));
% if a_max > 0.05
%     iLimit = cleanData.timeSeries > 0.05;
%     cleanData.timeSeries(iLimit) = 0.05;
%     iLimit = cleanData.timeSeries < -0.05;
%     cleanData.timeSeries(iLimit) = -0.05;
% end

%% Remove data prior to spike
[a_max,maxS] = max(max(abs(cleanData.timeSeries)));
[~,maxI] = max(abs(cleanData.timeSeries(:,maxS)));
if a_max > 0.05
    startI = maxI+10000; %new starting index
    cleanData.timeSeries = cleanData.timeSeries(startI:end,:);
    cleanData.timeStamps = cleanData.timeStamps(startI:end,:);
    cleanData.nSamples = size(cleanData.timeSeries,1);
    cleanData.spikesFound = true;
else
    startI = 10000; %new starting index
    cleanData.timeSeries = cleanData.timeSeries(startI:end,:);
    cleanData.timeStamps = cleanData.timeStamps(startI:end,:);
    cleanData.nSamples = size(cleanData.timeSeries,1);
    cleanData.spikesFound = false;
end

%% Find dead channels
[deadI,s] = findDeadChannels(cleanData.timeSeries);
cleanData.deadChannels = deadI;

%% Store channel strength
cleanData.channelStrength = s;






