% This is the main script for processing both kinect and accelerometer data
% to extract gait parameters.
% v1 is designed to work with JSON file outputs from the C++ implementation
% of the Azure Kinect body tracking SDK.

% clear; clc; close all;
    
% Get Kinect Data
filename = "Rafael_Regular_Gait_Body_Weight_Trial_1.json";  % Load filename
[kinect_data, trial_num, walking_speed, walking_weight, walker_name,...
    kinect_file] = get_kinect_data(filename);
    
% Time vector
[time_sec, kinect_fs] = kinect_time(kinect_data);

% Joints position
joint_positions = kinect_joints_positions(kinect_data);

% Get ankles, feet, and pelvis positions
[left_ankle, right_ankle, left_foot, right_foot, pelvis, left_hip, right_hip] = ...
    kinect_leg_positions(joint_positions);
 
% Extract footsteps
[left_foot_HS, left_foot_TO, right_foot_HS, right_foot_TO] =...
    kinect_steps(left_ankle, right_ankle, pelvis, time_sec,kinect_fs, ...
    walking_speed, trial_num);

% Extract walking distance
[walking_dist, walking_time, inst_kinect_gs, kinect_gs,gait_dir] = ...
    kinect_walking_distance(pelvis, time_sec);
    
% Save Data
% kinect_file = kinect_file(1:end-5) + ".xls";
% xlswrite(kinect_file, left_ankle, right_ankle, left_foot, right_foot, pelvis,...
% left_foot_inds, right_foot_inds, walking_dist, walking_time);

%% Extra plots
% Z - Direction Ankle Plot
% figure; hold on; grid on;
% plot(kinect_time, left_ankle(:,3)./1000, 'linewidth', 2.5)
% plot(kinect_time, right_ankle(:,3)./1000, 'linewidth',2.5)
% %title('Regular Gait ankle positions (Z-direction)', 'FontSize', 18)
% legend('Left ankle', 'right ankle', 'FontSize', 18)
% xlabel('Time (s)', 'FontSize', 18)
% ylabel('Position from Kinect (m)', 'FontSize', 18)
% ax = gca;
% xlim([15,30])
% ax.FontSize = 16;

% Z - Direction Pelvis Plot
% figure; hold on; grid on;
% plot(kinect_time, pelvis(:,3)./1000, 'linewidth', 2.5)
% title('Regular gait, pelvis position (Z-direction)', 'FontSize', 18)
% xlabel('Time (s)', 'FontSize', 18)
% ylabel('Position from kinect (m)', 'FontSize', 18)
% ax = gca;
% ax.FontSize = 16;

%% Accelerometers

% Get file 
file = kinect_file(1:end-5) + ".mat";
all_files = dir("C:\Users\rgoncalves\Documents\gait-analysis\Data\" + file);
accel_filename = all_files.name;

% Load data and normalized by sensor sensitivzity
[X,t,fs, stopwatch] = load_data("C:\Users\rgoncalves\Documents\gait-analysis\Data\" + accel_filename); 

% Detrend data
X = detrend_movmean(X,fs); % Detrends X with a moving average of window size fs

% Align Signals
X = align_peaks(X, t, fs);

% Detect Events
window_size = round(0.02 * fs); 
footstep_dur = 0.02; % in secs
peak_distance = 0.3 * fs;

[E,inds, SNR] = detect_events_SNR(X, window_size, peak_distance, fs);

% Plot Detected Footsteps
figure; hold on;                                             
plot(t,X(:,4), '-k', 'linew', 1.2);
plot(t(inds),zeros(length(inds),1),'or','MarkerSize',5, 'LineWidth',10); 
xlabel('Time (s)', 'FontSize', 18)
ylabel('Acceleration (m/s^2)', 'FontSize', 18)
legend('Acceleration', 'Detected Footsteps', 'FontSize', 18)
hold off;

%% Localization Trials

% Sensor Coordinates
x_sens = [0, 1.50,    0, 1.50,    0, 1.50];  % x - coordinates for s1, s2, ..., s5,s6
y_sens = [0,    0, 3.50, 3.50, 7.00, 7.00];  % y - coordinates for s1, s2, ..., s5,s6

% Least Squares Localization
[x_fit,y_fit] = localize_footsteps(E,inds,x_sens,y_sens); 

% Save results
% filename = accel_filename(1:end-4) + "_Footstep_Locations";
% save(filename, 'x_fit', 'y_fit');

%% Gait Parameters Extraction
% Step Length Variability
% Kinect
[step_length, step_length_CV, left_step_CV, right_step_CV] = ...
    kinect_step_var(left_foot_HS, left_foot_TO, right_foot_HS, ...
    right_foot_TO, left_foot, right_foot)

% Accel
[accel_step_length, accel_step_CV] = ...
    accel_step_var(y_fit, trial_num)

% Save results
% filename = accel_filename(1:end-4) + "_Step_Length";
% save(filename, 'step_length','step_length_CV','left_step_CV',...
%     'right_step_CV', 'accel_step_length', 'accel_step_CV')


% Gait Speed
% Accelerometer gait speed
[step_length_gs, loc_gs, mean_loc_gs] = ...
    accel_gait_speed(E,inds,t,step_length,y_fit,trial_num)
    
 
% % Save results
% filename = accel_filename(1:end-4) + "_Gait_Speed";
% save(filename,'stopwatch_gs','step_length_gs','loc_gs', ...
% 'mean_loc_gs', 'kinect_gs')


% Cadence
[accel_cad, kinect_cad] = ...
    cadence(inds, t,left_foot_HS, right_foot_HS, time_sec)

% Save results
% filename = accel_filename(1:end-4) + "_Cadence";
% save(filename, 'accel_cad', 'kinect_cad')
  

% Stride Time Variability
% Accel.
[tot_str_time_CV] = accel_stride_time_var(inds,t)

% Kinect
[kinect_left_str_CV, kinect_right_str_CV, tot_kinect_str_time_CV] = ...
    kinect_stride_time_var(left_foot_HS, right_foot_HS, time_sec)

% Save results
% filename = accel_filename(1:end-4) + "_Stride_Time_Var";
% save(filename, 'tot_str_time_CV', 'tot_kinect_str_time_CV')    


% Stride Length Variability
% Kinect
[stride_length, kinect_str_length_CV, left_str_CV, right_str_CV] = ...
    kinect_stride_len_var(left_foot_HS, right_foot_HS, left_foot, right_foot)

% Accel
[accel_str_length, accel_str_CV, accel_left_str_CV, accel_right_str_CV] = ...
    accel_stride_len_var(y_fit, trial_num)

% Save results 
% filename = accel_filename(1:end-4) + "_Stride_Length";
% save(filename, 'accel_str_CV', 'kinect_str_length_CV')