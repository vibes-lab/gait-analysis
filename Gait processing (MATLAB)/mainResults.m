load('Other Data/GaitSp_Cadence_Agreggation_all.mat')

patRange = 1:30;
%% Histograms
figure;
hold on;
histogram(accel_gs_reg(patRange),15)
histogram(kin_gs_reg(patRange),15)
histogram(sw_gs_reg(patRange),15)
legend('accel','kinect','stopwatch')
xlabel('Mean gait speed (m/s)')
title('Regular Gait Speed')
set(gca,'FontSize',16)

figure;
hold on;
histogram(accel_gs_fast(patRange),15)
histogram(kin_gs_fast(patRange),15)
histogram(sw_gs_fast(patRange),15)
legend('accel','kinect','stopwatch')
xlabel('Mean gait speed (m/s)')
title('Fast Gait Speed')
set(gca,'FontSize',16)

figure;
hold on;
histogram(accel_cad_reg(patRange),15)
histogram(kin_cad_reg(patRange),15)
legend('accel','kinect')
xlabel('Mean cadence (steps per minute)')
title('Regular Cadence')

figure;
hold on;
histogram(accel_cad_fast(patRange),15)
histogram(kin_cad_fast(patRange),15)
legend('accel','kinect')
xlabel('Mean cadence (steps per minute)')
title('Fast Cadence')
set(gca,'FontSize',16)

%% Patient Comparison
patientID = patRange;

figure;
hold on;
plot(patientID,accel_gs_reg(patRange),'o','LineWidth',2)
plot(patientID,kin_gs_reg(patRange),'x','LineWidth',2)
plot(patientID,sw_gs_reg(patRange),'v','LineWidth',2)
legend('accel','kinect','stopwatch')
ylabel('Mean gait speed (m/s)')
xlabel('Patient ID')
title('Regular Gait Speed')
set(gca,'FontSize',16)

figure;
hold on;
plot(patientID,accel_gs_fast(patRange),'o','LineWidth',2)
plot(patientID,kin_gs_fast(patRange),'x','LineWidth',2)
plot(patientID,sw_gs_fast(patRange),'v','LineWidth',2)
legend('accel','kinect','stopwatch')
ylabel('Mean gait speed (m/s)')
xlabel('Patient ID')
title('Fast Gait Speed')
set(gca,'FontSize',16)

figure;
hold on;
plot(patientID,accel_cad_reg(patRange),'o','LineWidth',2)
plot(patientID,kin_cad_reg(patRange),'x','LineWidth',2)
legend('accel','kinect')
ylabel('Mean Cadence (steps/min)')
xlabel('Patient ID')
title('Regular Cadence')
set(gca,'FontSize',16)

figure;
hold on;
plot(patientID,accel_cad_fast(patRange),'o','LineWidth',2)
plot(patientID,kin_cad_fast(patRange),'x','LineWidth',2)
legend('accel','kinect')
ylabel('Mean Cadence (steps/min)')
xlabel('Patient ID')
title('Fast Cadence')
set(gca,'FontSize',16)