function [time_in_sec, fs] = kinect_time(data)
% Get time vector from Kinect

frames_num = length(data.frames);
time_in_sec = zeros(frames_num, 1);

for i = 1:length(data.frames)
    time_in_sec(i) = data.frames(i).timestamp_usec / 10^6; % Convert time 
    % to seconds.
end

fs = 1/mode(diff(time_in_sec)); % Sampling rate
end