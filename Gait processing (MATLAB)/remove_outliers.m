function [filtered_energy, filtered_inds] = remove_outliers(E, inds, fs, dist_threshold)
%{
    Remove false footstep detection.
    
    Footsteps happen in an almost constant rate for this experiment, so any
    detected footstep that is too close or too far away from the average
    step time is an outlier or false detection. This function removes them
    by deleting outliers according to dist_threshold.

    Inputs:
    * E - energy matrix of detected footsteps
    * inds - indexes of detected footsteps
    * fs - sampling rate
    * dist_threshold - max distance allowed between footsteps (in seconds).

    Output:
    * filtered_energy - footstep energy matrix without outliers.
    * filtered_inds - indexes of filtered footsteps.
%}

% Max allowed distance between footsteps in samples
max_peak_dist = dist_threshold * fs;

% Checking first and last peaks
while abs(inds(1) - inds(2)) > max_peak_dist
    inds(1) = [];
    E(1,:) = [];
end

while abs(inds(end) - inds(end-1)) > max_peak_dist
    inds(end) = [];
    E(end,:) = [];
end

filtered_energy = E;
filtered_inds = inds; 