function [X, delay] = align_peaks(x_original, fs, deadChannels)
%{
    Synchronizes signals from the two cDaqs through cross correlation
    If sensors 6 or 7 are not dead, it uses those two because they are in
    the same location. Otherwise, it choses the two strongest sensors on
    each DAQ.
    
%}

if nargin <3
    deadChannels = false(7,1);
end

X = x_original;

if ~deadChannels(6) && ~deadChannels(7)
    %if neither channels 6 or 7 are dead
    delay = finddelay(x_original(:,7), x_original(:,6));  % Finds the delay in sec.
elseif ~deadChannels(2) && ~deadChannels(5)
    delay = finddelay(x_original(:,2), x_original(:,5));  % Finds the delay in sec.
elseif ~deadChannels(3) && ~deadChannels(4)
    delay = finddelay(x_original(:,3), x_original(:,4));  % Finds the delay in sec.    
elseif ~deadChannels(1) && ~deadChannels(6)
    delay = finddelay(x_original(:,1), x_original(:,6));  % Finds the delay in sec.
elseif ~deadChannels(1) && ~deadChannels(7)
    delay = finddelay(x_original(:,1), x_original(:,7));  % Finds the delay in sec.
else
    delay = 0;
end
    
if delay >= 0
    sens4_shifted =  circshift(x_original(:,4), -delay, 1);
    sens5_shifted =  circshift(x_original(:,5), -delay, 1);

    if ~deadChannels(6)
        sens6_shifted =  circshift(x_original(:,6), -delay, 1);
    else
        sens6_shifted =  circshift(x_original(:,7), -delay, 1);
    end

    X(:,4) = sens4_shifted;
    X(:,5) = sens5_shifted;
    X(:,6) = sens6_shifted;
    X(:,7) = [];  % Delete sensor 7 as it is not used in the analysis
    
elseif delay < 0
    sens1_shifted =  circshift(x_original(:,1), delay, 1);
    sens2_shifted =  circshift(x_original(:,2), delay, 1);
    sens3_shifted =  circshift(x_original(:,3), delay, 1);
    
    X(:,1) = sens1_shifted;
    X(:,2) = sens2_shifted;
    X(:,3) = sens3_shifted;
    
    X(:,7) = [];  % Delete sensor 7 as it is not used in the analysis
else
    X = x_original;
    X(:,7) = [];

end

%% Plot sensors 5 and 6
% figure; hold on;
% plot(time, sens5_shifted)
% plot(time, x_original(:,6))
% legend("Sensor 5", "Sensor 6")
% corr(sens5_shifted, x_original(:,6))

%% Find the delay between sensors
delay = delay / fs;

% Plot aligned signals
% figure; hold on;
% plot(time,X)
% title("Adjusted Signals")
% legend("Sens 1", "Sens 2", "Sens 3", "Sens 4", "Sens 5", "Sens 6")