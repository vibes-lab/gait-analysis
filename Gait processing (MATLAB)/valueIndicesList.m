function I = value2Index(values,array)
% Returns the indeces of ARRAY corresponding to each value in VALUES. If
% value is not found, index is 0;

N = length(values);
for k = 1:N
    Imatch = find(array == values);
    if isempty(Imatch)
        I(k) = 0;
    else
        I(k) = Imatch(1)
    end
end