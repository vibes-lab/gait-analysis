function [accel_cad, kinect_cad] = ...
    cadence(inds, t, left_HS, right_HS, kinect_time)

%{
    Obtains cadence (# steps per min) from the accels., kinect, and stopwatch.
    
    Inputs:
    * Footsteps inds from accel.
    * Accel time vector.
    * Left and right foot heel strikes from the kinect.
    * Kinect time vector.
    
    Outputs - mean cadence metrics for the entire walk from the accels, 
    kinect, and stopwatch (if used).
 
%}

%% Accel cadence

% Total Cadence
step_time = t(inds); % Time stamps of footsteps
step_rate = diff(step_time);
step_rate_cln = rmoutliers(step_rate,'mean');

accel_cad = 60/mean(step_rate_cln); % Units: Steps/minute

%% Kinect cadence
step_count = length(left_HS) + length(right_HS); 
footstep_times = sort([kinect_time(left_HS); kinect_time(right_HS)]);
total_walking_time = max(footstep_times) - min(footstep_times);

kinect_cad = (step_count / total_walking_time) * 60;
end