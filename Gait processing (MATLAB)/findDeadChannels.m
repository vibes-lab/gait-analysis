function [deadI,s] = findDeadChannels(data)

s = std(data,0,1);
K = kurtosis(data,0,1);

%outliers = s < mean(s)-2*std(s);
deadI = K < 10 | s < 0.05e-3 ;