function [step_length_gs, loc_gs, mean_loc_gs] = ...
    accel_gait_speed(E, inds, t, step_length,  y_fit, gait_dir)
%{
    Get gait speed from accelerometer data

    This fuction calculates gait speed according to the detected footsteps
    in the convex-hull. 
    
    It also calculates the gait speed using the average step length from
    the kinect and the step detection vector inds from the accel to improve
    accuracy and precision.
    
    Inputs:
    * E - footstep RMS matrix 
    * inds - indexes of detected footsteps
    * t - total time vector from the accel.
    * stopwatch_time - stopwatch time measurements taken from the
    experiment.
    * step_length - average step length obtained from the kinect.
    * y-fit - localized footsteps from the energy-based algorithm.
    * trial_num - trial number 
    
    Outputs:
    * step_length_gs - gait speed using kinect step length combined with
    accel time vector.
    * loc_gs - gait speed calculated using the first and the last localized
    footstep in the convex-hull.
    * mean_loc_gs - average gait speed using every localized footstep.
%}            

%% Vibration-Based Gait Speed
% Estimated gs using kinect step length
total_distance = step_length * length(E);
total_time = abs(t(inds(1)) - t(inds(end)));

step_length_gs = total_distance / total_time;

%% Gait Speed using Localization Results
% Find first and last steps inside convex-hull
first_ind = 1; % Assume it takes 3 steps to get inside the convex hull
last_ind = length(y_fit); % Assume the last 4 are outside

% Adjust vectors according to detected steps
y_fit = y_fit(first_ind:last_ind); 
detected_inds = inds(first_ind:last_ind);
time = t(detected_inds);

% Calculate gait speed
loc_gs = abs(y_fit(end) - y_fit(1)) / abs(time(end) - time(1));

% Calculate average gait speed 
y = abs(diff(y_fit));
diff_t = diff(time);
gait_speed = y./ diff_t;
mean_loc_gs = mean(gait_speed);
    
end