function [accel_step_length, accel_step_CV] = ...
    accel_step_var(y_fit, trial_num)

    %% Find first and last steps inside convex-hull
    if mod(trial_num, 2) ~= 0 % Odd numbered trial
        first_steps = find(y_fit < 0.01);
        last_steps = find(y_fit > 6.99);
        
        if isempty(first_steps)
            first_steps = 1;
        end

        if isempty(last_steps)
            last_steps = length(y_fit);
        end
        
        first_ind = first_steps(end);
        last_ind = last_steps(1);
    else
        first_steps = find(y_fit > 6.99);
        last_steps = find(y_fit < 0.01);
        
        if isempty(last_steps)
            last_steps = length(y_fit);
        end
        
        if isempty(first_steps)
            first_steps = 1;
        end
        
        first_ind = first_steps(end);
        last_ind = last_steps(1);
    end

    % Adjust vectors according to detected steps
    y_fit = y_fit(first_ind:last_ind);
   

    %% Get CV
    step_lengths = abs(diff(y_fit));
    accel_step_length = mean(step_lengths);
    total_std = std(step_lengths);

    accel_step_CV = (total_std / accel_step_length) * 100;
end